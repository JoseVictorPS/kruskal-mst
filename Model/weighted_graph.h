//
// Created by josevictor-ps on 25/07/19.
//

#ifndef UNTITLED_WEIGHTED_GRAPH_H
#define UNTITLED_WEIGHTED_GRAPH_H

#include "vertice.h"
#include "../Controller/weighted_graph/weighted_graph_exception.h"

class Graph{
private:
    vertice*g;

public:
    void insert_neighbors(int source, int destiny, int weight);

    void remove_neighbors(int source, int destiny);

    bool is_empty();

    vertice * search(int info);

    vertice * get_vertices();

    ~Graph();

    void remove(int info);

    void insert(int info);

    int size();

    int n_of_edges();

    Graph();
};

#endif //UNTITLED_WEIGHTED_GRAPH_H
