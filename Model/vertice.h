//
// Created by josevictor-ps on 20/07/19.
//
#ifndef KRUSKALS_VERTICE_H
#define KRUSKALS_VERTICE_H

#include "edge.h"

class Graph;

struct vertice {

private:

    friend class Graph;

    int info;
    vertice *next; //Pointer to next vertice in Linked List
    edge * neighbors; //Linked List for the node's neighbors

    void remove_from_neighbors(vertice*g, vertice*target);

    void insert_neighbors(vertice*g, vertice*target, int weight);

    void remove_neighbors(vertice*g, vertice*target);

    vertice* delete_vertice(vertice * g);

    vertice*remove(vertice*g, int info);

    vertice * insert(vertice * g, int info);

public:
    vertice * search(vertice*g, int info);

    vertice * get_next();

    edge * get_neighbors();

    int get_info();

    vertice();
};

#endif //KRUSKALS_VERTICE_H
