//
// Created by josevictor-ps on 21/07/19.
//
//Algorithm as followed in this site:
//https://www.geeksforgeeks.org/kruskals-minimum-spanning-tree-algorithm-greedy-algo-2/

#ifndef UNTITLED_KRUSKAL_MST_H
#define UNTITLED_KRUSKAL_MST_H

#include "weighted_graph.h"

class Kruskal {
private:

    struct kruskal_node{
        int source;
        int destiny;
        int weight;
    };

    Graph*g;

    struct kruskal_node* kruskal_array();

    static int kruskal_array_cmp(const void *a, const void *b);

    bool detect_cycle(Graph* mst);

public:
    Graph* minimum_spanning_tree();

    void set_graph(Graph*g);

    Graph* get_graph();

    ~Kruskal();

    Kruskal();
};



#endif //UNTITLED_KRUSKAL_MST_H
