//
// Created by josevictor-ps on 20/07/19.
//
#ifndef KRUSKALS_EDGE_H
#define KRUSKALS_EDGE_H

#include <stdlib.h>
#include <stdio.h>

struct vertice;

struct edge { // List for edges in graph

private:

    friend struct vertice;

    vertice * node; // Generic pointer
    int weight;
    struct edge * next; // Pointer to next edge in list

    edge* insert( edge * f, vertice * node, int weight);

    bool is_empty(edge * f);

    edge* free_edges(edge * f);

    edge* remove(edge*f, vertice*p);

public:
    edge * search(edge*f, vertice*p);

    vertice* get_node();

    edge* get_next();

    int get_weight();

    edge();
};

#endif //KRUSKALS_EDGE_H
