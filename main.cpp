#include <iostream>

#include "Model/kruskal_mst.h"
#include "View/interface_user.h"


int main() {

    //Graph*meu_grafo = View::build_graph_UX();

    //View::insert_neighbors_UX(meu_grafo);

    //meu_grafo = View::remove_from_UX(meu_grafo);

    Graph*meu_grafo = new Graph;

    /*meu_grafo->insert(0);
    meu_grafo->insert(1);
    meu_grafo->insert(2);
    meu_grafo->insert(3);
    meu_grafo->insert(4);

    meu_grafo->insert_neighbors(2, 4, 2);
    meu_grafo->insert_neighbors(0, 1, 1);
    meu_grafo->insert_neighbors(0, 3, 3);
    meu_grafo->insert_neighbors(3, 4, 1);

    View::print_graph(meu_grafo);
    printf("\n\n");

    meu_grafo->remove_neighbors(0, 3);

    View::print_graph(meu_grafo);*/

    meu_grafo->insert(0);
    meu_grafo->insert(1);
    meu_grafo->insert(2);
    meu_grafo->insert(3);
    meu_grafo->insert(4);
    meu_grafo->insert(5);

    meu_grafo->insert_neighbors(0, 1, 1);
    meu_grafo->insert_neighbors(0, 3, 3);
    meu_grafo->insert_neighbors(1, 3, 4);
    meu_grafo->insert_neighbors(1, 2, 7);
    meu_grafo->insert_neighbors(3, 2, 5);
    meu_grafo->insert_neighbors(3, 4, 1);
    meu_grafo->insert_neighbors(2, 4, 2);
    meu_grafo->insert_neighbors(2, 5, 9);
    meu_grafo->insert_neighbors(4, 5, 6);

    Kruskal* kruskal = new Kruskal;
    kruskal->set_graph( meu_grafo );

    Graph*mst = kruskal->minimum_spanning_tree();

    View::print_graph(mst);

    delete kruskal;
    delete mst;
    //delete meu_grafo;

    return 0;
}