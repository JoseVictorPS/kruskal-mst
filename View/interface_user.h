//
// Created by josevictor-ps on 21/07/19.
//

#ifndef UNTITLED_INTERFACE_USER_H
#define UNTITLED_INTERFACE_USER_H

#include "../Model/weighted_graph.h"

class View {
private:
    static void print_neighbors(edge*e, int peso);

    static void print_vertice(vertice * g);

public:
    static void insert_neighbors_UX(Graph*g);

    static Graph * build_graph_UX();

    static Graph * remove_from_UX(Graph*g);

    static void print_graph(Graph*g);
};

#endif //UNTITLED_INTERFACE_USER_H
