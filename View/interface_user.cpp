//
// Created by josevictor-ps on 21/07/19.
//

#include "interface_user.h"

void View::insert_neighbors_UX(Graph*g) {
    printf("Want to insert a neighbor? ");
    int answer, destiny, source, weight;
    scanf("%d", &answer);

    while(answer) {
        printf("What's the neighbor's info? ");
        scanf("%d", &destiny);
        printf("Whose neighbor is this? ");
        scanf("%d", &source);
        printf("What's the weight of the edge? ");
        scanf("%d", &weight);

        try{
            g->insert_neighbors(source, destiny, weight);
        }
        catch(nodeNotFoundException& e) {
            printf(e.msg());
            printf("\n");
        }
        catch(neighborAlreadyExistsException& e) {
            printf(e.msg());
            printf("\n");
        }
        catch(selfNeighborException& e) {
            printf(e.msg());
            printf("\n");
        }

        for(vertice* iterator = g->get_vertices(); iterator != NULL; iterator = iterator->get_next()) {
            print_vertice(iterator);
        }

        printf("Insert another neighbor? ");
        scanf("%d", &answer);
    }
}

Graph * View::build_graph_UX() {
    auto g = new Graph;

    printf("Want to insert a vertice in the graph? ");
    int answer, info;
    scanf("%d", &answer);

    while(answer) {
        printf("What's the vertice's info? ");
        scanf("%d", &info);

        try{
            g->insert(info);
        }
        catch(nodeAlreadyExistsException& e) {
            printf(e.msg());
            printf("\n");
        }

        for(vertice* iterator = g->get_vertices(); iterator != NULL; iterator = iterator->get_next()) {
            print_vertice(iterator);
        }

        printf("Wanna insert another vertice? ");
        scanf("%d", &answer);
    }

    return g;
}

Graph * View::remove_from_UX(Graph*g) {
    printf("Want to remove a vertice from the graph? ");
    int answer, info;
    scanf("%d", &answer);

    while(answer) {
        printf("What's the vertice's info? ");
        scanf("%d", &info);

        try{
            g->remove(info);
        }
        catch (nodeNotFoundException& e) {
            printf(e.msg());
            printf("\n");
        }

        for(vertice* iterator = g->get_vertices(); iterator != NULL; iterator = iterator->get_next()) {
            print_vertice(iterator);
        }

        printf("Wanna remove another vertice? ");
        scanf("%d", &answer);
    }

    return g;
}

void View::print_neighbors(edge*e, int peso) {
    vertice* v = e->get_node();
    printf("(%d, weight: %d) ", v->get_info(), peso);
}

void View::print_vertice(vertice * g) {
    //Prints the info in the node
    printf("%d -> ", g->get_info());

    //Prints the neighbors
    for(edge* iterator = g->get_neighbors(); iterator != NULL; iterator = iterator->get_next()) {
        print_neighbors(iterator, iterator->get_weight());
    }
    printf("\n");
}

void View::print_graph(Graph*g) {
    for(vertice* iterator = g->get_vertices(); iterator != NULL; iterator = iterator->get_next()) {
        print_vertice(iterator);
    }
}