//
// Created by josevictor-ps on 20/07/19.
//

#include "../../Model/vertice.h"

//PUBLIC METHODS

edge* vertice::get_neighbors() { return neighbors; }

int vertice::get_info() { return info; }

vertice * vertice::get_next() {
    return next;
}

void vertice::remove_from_neighbors(vertice*g, vertice*target) {
    while(g) {
        //Removes all the nodes that points to target from the neighbors list
        g->neighbors = g->neighbors->remove(g->neighbors, target);
        g=g->next;
    }
}

void vertice::insert_neighbors(vertice*g, vertice*target, int weight) {
    //Inserts the node target as a neighbor of g, and g as a neighbor of target
    g->neighbors = g->neighbors->insert(g->neighbors, target, weight);
    target->neighbors = target->neighbors->insert(target->neighbors, g, weight);
}

void vertice::remove_neighbors(vertice*g, vertice*target) {
    //Removes the node target as a neighbor of g, and g as a neighbor of target
    g->neighbors = g->neighbors->remove(g->neighbors, target);
    target->neighbors = target->neighbors->remove(target->neighbors, g);
}

vertice * vertice::search(vertice*g, int info) {
    if(!g) return NULL;

    if(g->info==info) return g;
    return search(g->next, info);//Searches in the next node
}

vertice* vertice::delete_vertice(vertice * g) {
    if(!g) return NULL;

    vertice*helper;
    while(g) {
        helper = g;//Sets helper to the current node
        g = g->next;//Updates iterator node
        helper->neighbors = helper->neighbors->free_edges(helper->neighbors);//Frees list of neighbors
        free(helper);//Frees the node
    }

    return NULL;
}

vertice * vertice::insert(vertice * g, int info) {
    //Allocates memory and sets the fields from the parameters
    vertice *node = (vertice *) malloc(sizeof(vertice));
    node->next = g;
    node->info = info;

    //Return the node to be the first in the list of vertices, like a Linked List
    return node;
}

vertice* vertice::remove(vertice*g, int info) {
    vertice*iterator = g, *previous=NULL;

    //Trying to find the node to remove
    while((iterator) && (iterator->info!=info)) {
        previous=iterator;
        iterator=iterator->next;
    }

    //Case which haven't found
    if(!iterator) return g;

    //Case which is the first in the list
    if(!previous) g=g->next;

    //Ties the next node
    else previous->next = iterator->next;

    //Removes the list of edges of the to be deleted vertice
    iterator->neighbors = iterator->neighbors->free_edges(iterator->neighbors);

    //Removes the edges that points to the to be deleted vertice in the others lists of edges
    remove_from_neighbors(g, iterator);

    //Deletes the vertice
    free(iterator);

    return g;
}

vertice::vertice() {
    info = 0;
    next = NULL;
    neighbors = NULL;
}