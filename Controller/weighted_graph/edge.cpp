//
// Created by josevictor-ps on 20/07/19.
//

#include "../../Model/edge.h"

edge* edge::insert( edge * e, vertice * node, int weight) {
    //Allocates memory and sets all the fields with the parameters
    edge * n = (edge *)malloc(sizeof(edge));
    n->node = node;
    n->next = e;
    n->weight = weight;

    //Returns the node to be the first one, like a linked list
    return n;
}

bool edge::is_empty(edge * e) {
    return (e == NULL);
}

edge* edge::free_edges(edge * e) {
    if(!e) return NULL;

    edge * iteration = e;
    edge * helper;
    while(iteration) {
        helper = iteration->next; //Saves the next node
        free(iteration);
        iteration = helper;
    }

    return NULL;
}

edge* edge::remove(edge*e, vertice*p) {
    edge*iteration = e, *previous=NULL;

    //Trying to find the node
    while((iteration) && (iteration->node != p)) {
        previous=iteration;
        iteration=iteration->next;
    }

    //Case which the node wasn't found
    if(!iteration) return e;

    //Case which the node is the first
    if(iteration == e) e = iteration->next;

    //Ties the next node
    else previous->next = iteration->next;

    free(iteration);
    return e;
}

edge * edge::search(edge*e, vertice*p) {
    edge*iteration=e;

    while((iteration) && (iteration->node!=p)) iteration=iteration->next;

    return iteration;
}

edge * edge::get_next() {
    return next;
}

int edge::get_weight() {
    return weight;
}

vertice* edge::get_node() {
    return node;
}

edge::edge() {
    node = NULL;
    next = NULL;
    weight = 0;
}