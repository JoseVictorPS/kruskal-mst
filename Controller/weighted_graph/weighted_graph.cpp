//
// Created by josevictor-ps on 25/07/19.
//

#include "../../Model/weighted_graph.h"

void Graph::insert_neighbors(int source, int destiny, int weight){
    //If the node to be the neighbor exists
    if(vertice*target = search(destiny)) {

        //If the node that we're inserting a neighbor exists
        if(vertice*from = search(source)){

            //If the node is not a neighbor yet
            if(from->get_neighbors()->search(from->get_neighbors(), target) == NULL){

                //If destiny is not the same as the source
                if(destiny != from->get_info()){
                    from->insert_neighbors(from, target, weight);
                }

                else {
                    throw selfNeighborException();
                }
            }

            else {
                throw neighborAlreadyExistsException();
            }
        }

        else {
            throw nodeNotFoundException();
        }
    }

    else {
        throw nodeNotFoundException();
    }
}

void Graph::remove_neighbors(int source, int destiny) {
    //If the neighbor we're removing exists
    if(vertice*target = search(destiny)) {

        //If the node that we're removing from exists
        if(vertice*from = search(source)){

            //If the destiny node is a neighbor from source
            if(from->get_neighbors()->search(from->get_neighbors(), target) != NULL){
                from->remove_neighbors(from, target);
            }

            else {
                throw notANeighborException();
            }
        }

        else {
            throw nodeNotFoundException();
        }
    }

    else {
        throw nodeNotFoundException();
    }
}

bool Graph::is_empty() {
    return g == NULL;
}

vertice * Graph::search(int info){
    return g->search(g, info);
}

Graph::~Graph() {
    g->delete_vertice(g);
}

void Graph::remove(int info) {
    //If the node to be removed is there
    if(search(info) != NULL){
        g = g->remove(g, info);
    }
    else {
        throw nodeNotFoundException();
    }
}

void Graph::insert(int info) {
    //If there's a node with the same info already
    if(search(info) == NULL){
        g = g->insert(g, info);
    }
    else {
        throw nodeAlreadyExistsException();
    }
}

vertice * Graph::get_vertices() {
    return g;
}

int Graph::size() {
    int counted = 0;
    for(vertice*iterator = g; iterator!=NULL; iterator=iterator->get_next()) {
        counted++;
    }
    return counted;
}

int Graph::n_of_edges() {
    vertice*counted = NULL;
    vertice*iterator = g;
    int num_edges = 0;

    for(; iterator != NULL; iterator = iterator->get_next()) {
        try{
            counted = counted->insert( counted, iterator->get_info() );
            //counted->insert( counted, iterator->get_info() );
        }
        catch (...){}

        edge*test_edge = iterator->get_neighbors();
        for(; test_edge != NULL; test_edge = test_edge->get_next()) {
            if(counted->search(counted, test_edge->get_node()->get_info()) == NULL) {
                num_edges++;
            }
        }

    }

    counted->delete_vertice(counted);
    return  num_edges;
}

Graph::Graph() {
    g = NULL;
}