//
// Created by josevictor-ps on 25/07/19.
//

#ifndef UNTITLED_WEIGHTED_GRAPH_EXCEPTION_H
#define UNTITLED_WEIGHTED_GRAPH_EXCEPTION_H

#include <exception>

class nodeNotFoundException : public std::exception {
public:
    const char * msg () {
        return "Node not found";
    }
};

class nodeAlreadyExistsException: public std::exception {
public:
    const char * msg () {
        return "Node already exists";
    }
};

class neighborAlreadyExistsException: public std::exception {
public:
    const char * msg () {
        return "Neighbor already exists";
    }
};

class selfNeighborException: public std::exception {
public:
    const char * msg () {
        return "Can't neighbor itself";
    }
};

class notANeighborException: public std::exception {
public:
    const char * msg () {
        return "The node is not a neighbor";
    }
};

#endif //UNTITLED_WEIGHTED_GRAPH_EXCEPTION_H
