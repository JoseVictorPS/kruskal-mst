//
// Created by josevictor-ps on 26/07/19.
//

#include "../Model/kruskal_mst.h"
#include "../View/interface_user.h"

struct Kruskal::kruskal_node* Kruskal::kruskal_array() {
    int num_edges = g->n_of_edges();
    struct kruskal_node* array = (kruskal_node*)malloc(sizeof(struct kruskal_node) * num_edges);
    int index = 0;

    Graph*counted = new Graph;
    vertice*iterator = g->get_vertices();

    for(; iterator != NULL; iterator = iterator->get_next()) {
        try{
            counted->insert( iterator->get_info() );
        }
        catch (...){}

        edge*test_edge = iterator->get_neighbors();
        for(; test_edge != NULL; test_edge = test_edge->get_next()) {

            if(counted->search(test_edge->get_node()->get_info()) == NULL) {
                array[index].weight = test_edge->get_weight();
                array[index].source = iterator->get_info();
                array[index].destiny = test_edge->get_node()->get_info();
                index++;
            }
        }

    }

    qsort(array, num_edges, sizeof(struct kruskal_node), kruskal_array_cmp);

    delete counted;
    return  array;
}

int Kruskal::kruskal_array_cmp(const void *a, const void *b) {
    struct kruskal_node* ptr_a = (struct kruskal_node*)a;
    struct kruskal_node* ptr_b = (struct kruskal_node*)b;
    struct kruskal_node elem_a = *ptr_a;
    struct kruskal_node elem_b = *ptr_b;

    return elem_a.weight - elem_b.weight;
}

Graph* Kruskal::minimum_spanning_tree() {
    struct kruskal_node* array = kruskal_array();
    int index = 0;
    int array_size = g->n_of_edges();

    Graph* mst = new Graph;

    struct kruskal_node test_node;
    while(index < array_size && mst->n_of_edges() < g->size()) {
        test_node = array[index];

        try{
            mst->insert(test_node.source);
        }
        catch (...){}
        try{
            mst->insert(test_node.destiny);
        }
        catch (...){}
        try{
            mst->insert_neighbors(test_node.source, test_node.destiny, test_node.weight);
        }
        catch (...){}

        if(detect_cycle(mst)) {
           try{
               //DEBUG
               /*printf("aresta: %d max: %d\n", mst->n_of_edges(), g->size());
               View::print_graph(mst);
               printf("%d ", mst->get_vertices()->get_info());
               printf("%d ", mst->get_vertices()->get_next()->get_info());
               printf("%d ", mst->get_vertices()->get_next()->get_next()->get_info());*/
               mst->remove_neighbors(test_node.source, test_node.destiny);
               //DEBUG
               /*printf("\n\n");
               printf("aresta: %d max: %d\n", mst->n_of_edges(), g->size());
               printf("%d ", mst->get_vertices()->get_info());
               printf("%d ", mst->get_vertices()->get_next()->get_info());
               printf("%d\n", mst->get_vertices()->get_next()->get_next()->get_info());
               //View::print_graph(mst);
               printf("%d ", mst->get_vertices()->get_info());
               printf("%d ", mst->get_vertices()->get_next()->get_info());
               printf("%d ", mst->get_vertices()->get_next()->get_next()->get_info());*/

           }
           catch (...) {}
        }

        index++;
    }

    delete array;
    return mst;
}

void Kruskal::set_graph(Graph *g) {
    this->g = g;
}

Graph* Kruskal::get_graph() {
    return g;
}

Kruskal::~Kruskal() {
    delete g;
}

Kruskal::Kruskal() {
    g = new Graph;
}

bool Kruskal::detect_cycle(Graph* mst) {
    Graph * counted = new Graph;

    vertice * v_iterator = mst->get_vertices();
    edge * e_iterator;
    int info;
    for(; v_iterator != NULL; v_iterator = v_iterator->get_next()) {
        e_iterator = v_iterator->get_neighbors();

        for(; e_iterator != NULL; e_iterator = e_iterator = e_iterator->get_next()) {
            info = e_iterator->get_node()->get_info();

            if(counted->search( info ) != NULL) {

                if(counted->search( v_iterator->get_info() ) != NULL) {
                    delete counted;
                    return true;
                }
            }

            try{
                counted->insert( info );
            }
            catch (...) {}
        }
    }

    delete counted;
    return false;
}